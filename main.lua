DEBUGMODE = true

LIBS = 'data/lib/'
ASSETS = 'data/assets/'
SOURCE = 'data/src/'

ENTITIES = SOURCE..'entities/'
LEVELS = SOURCE..'levels/'
LOGIC = SOURCE..'logic/'
RENDERING = SOURCE..'rendering/'
INTERFACE = SOURCE..'interface/'
DEBUGUI = INTERFACE..'debug/'
SPRITES = ASSETS..'sprites/'
ANIMATIONS = ASSETS..'animations/'
function love.load()
  camera = require(RENDERING .. 'camera')
  map = require(LEVELS .. 'map').init()
end

function love.update(dt)
  map:update(dt)
end

function love.draw()
  camera:set()
  map:draw()
  camera:unset()
end
