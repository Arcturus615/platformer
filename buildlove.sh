#!/bin/bash
# Love builder

NAME="physics-test"
BUILD='./bin'
IGNORE='build.sh|README.md|love|bin|.vscode'

LOVEVERSION="love-0.10.2"

# LIN32BUILD="$NAME-lin32"
# LIN64BUILD="$NAME-lin64"
WIN32BUILD="$NAME-win32.exe"
WIN64BUILD="$NAME-win64.exe"

match () {
  awk -v value="$1" -v pattern="$2" '
      BEGIN {
          if (value ~ pattern) {
              exit 0
          } else {
              exit 1
          }
      }'
}

getIgnoredFiles() {
    while read -r file; do
        echo "$file"
        IGNORE="${IGNORE}|$file"
    done < ".gitignore"
    echo "$IGNORE"
}

processFiles () {
    getIgnoredFiles
    for filename in *; do
        if ! match "$filename" "$IGNORE"; then
            zip -9 -r "$BUILD/${NAME}.love" "./$filename"
        fi
    done
}

generateExe () {
    cat "./love/$LOVEVERSION-win32/love.exe" "$BUILD/${NAME}.love" > "$BUILD/win32/$WIN32BUILD"
    cat "./love/$LOVEVERSION-win64/love.exe" "$BUILD/${NAME}.love" > "$BUILD/win64/$WIN64BUILD"
}

populateLibs () {
    local olddir
    olddir=$(pwd)
    cd "./love/$LOVEVERSION-win32/" || exit 1
    for filename in *; do
        if [ "$filename" == "love.exe" ] || [ "$filename" == "lovec.exe" ]; then
            continue
        fi
        cp -u "$filename" "$olddir/$BUILD/win32/"
    done

    cd "$olddir" || exit 1
    cd "./love/$LOVEVERSION-win64/" || exit 1
    for filename in *; do
        if [ "$filename" == "love.exe" ] || [ "$filename" == "lovec.exe" ]; then
            continue
        fi
        cp -u "$filename" "$olddir/$BUILD/win64/"
    done
}

if [ -a "$BUILD/${NAME}.love" ]; then rm "$BUILD/${NAME}.love"; fi

mkdir -p $BUILD/lin32
mkdir -p $BUILD/lin64
mkdir -p $BUILD/win32
mkdir -p $BUILD/win64
processFiles
generateExe
populateLibs


exit 0
