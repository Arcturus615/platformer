local graphic = {}

function graphic.line(color, width, x1, y1, x2, y2, ...)
	love.graphics.push('all')
	love.graphics.setLineWidth(width)
	love.graphics.setColor(color)
	love.graphics.line(x1, y1, x2, y2, ...)
	love.graphics.pop()
end

function graphic.rectLine(color, x, y, width, height, line_width)
	love.graphics.push('all')
	love.graphics.setColor(color)
	love.graphics.setLineWidth(line_width)
	love.graphics.rectangle('line', x, y, width, height)
	love.graphics.pop()
end

function graphic.rectFill(color, x, y, width, height)
	love.graphics.push('all')
  love.graphics.setColor(color)
  love.graphics.rectangle('fill', x, y, width, height)
	love.graphics.pop()
end

function graphic.rectFillLine(color, x, y, width, height, line_width, line_color)
		love.graphics.push('all')
		love.graphics.setColor(color)
    love.graphics.rectangle('fill', x, y, width, height)
    love.graphics.setColor(line_color)
    love.graphics.setLineWidth(line_width)
    love.graphics.rectangle('line', x + line_width / 2, y + line_width / 2, width - line_width, height - line_width)
    love.graphics.pop()
end

function graphic.circleFill(color, x, y, radius, segments)
	love.graphics.push('all')
	love.graphics.setColor(color)
	love.graphics.circle('fill', x, y, radius, segments)
	love.graphics.pop()
end

function graphic.circleLine(color, line_width, x, y, radius, segments)
	love.graphics.push('all')
	love.graphics.setColor(color)
	love.graphics.setLineWidth(line_width)
	love.graphics.circle('line', x, y, radius, segments)
	love.graphics.pop()
end

function graphic.circleFillLine(color, line_width, line_color, x, y, radius, segments)
	love.graphics.push('all')
	love.graphics.setColor(color)
	love.graphics.setLineWidth(line_width)
	love.graphics.circle('line', x, y, radius, segments)
	love.graphics.setColor(line_color)
	love.graphics.circle('fill', x, y, radius, segments)
	love.graphics.pop()
end

function graphic.text(text, color, x, y, wrap, ...)
	-- ... is used for specifying a font to be used for this text. If formatting
	-- is required, use draw.textFormatting()
		love.graphics.push('all')
    if ... then love.graphics.setFont(...) end
    love.graphics.setColor(color)
    love.graphics.printf(text, x, y, wrap, 'left')
    love.graphics.pop()
end

function graphic.textFormatting(text, color, x, y, r, sx, sy, ox, oy, kx, ky, ...)
	-- Used for drawing text with special formatting or effects, if plain text
	-- is desired use draw.text()
	-- NOTE: Formatting is required if using this function!!
    love.graphics.push('all')
		if ... then love.graphics.setFont(font) end
    love.graphics.setColor(color)
    love.graphics.printf(text, x, y, 100, 'left')
    love.graphics.pop()
end

return graphic
