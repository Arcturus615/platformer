local Box = {}
Box.__index = Box
Box.__type = 'box'

local function isbox(object)
  assert(object.x and object.y and object.w and object.h,
    'Box: Box missing required values!')
  return true
end

local function init(x, y, w, h)
  return setmetatable(
    { 
      x = (x or 0),
      y = (y or 0),
      w = (w or 1),
      h = (h or 1)
    },
    Box)
end

function Box.getDiff(A, B)
end

return setmetatable(
  { init=init, isbox=isbox},
  { __call = function(_, ...) return init(...) end}
)