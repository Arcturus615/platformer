local qol = {}

function qol.clamp(min, value, max)
	if value < min then return min end
	if value > max then return max end
	return value
end

function qol.absclamp(value, max)
	max = math.abs(max)
	if value > max then
		if value < 0 then
			return max * -1
		else
			return max
		end
	end

	return value
end

function qol.getFilename(str)
	return str:match("([^/]-([^.]+))$")
end

function qol.clone(tbl)
	return {unpack(tbl)}
end

function qol.split(table, n)
	-- Given (table), the split function will return a new table
	-- with all the old tables values split into (n) subtables.
	--
	-- [n]: Determines the max number of keys that should be in a table,
	-- 			once a table reaches that number, a new table is generated.
	local newTable = {}
	local i = 1
	for _, v in ipairs(tbl) do
		if not newTable[i] then newTable[i] = {} end
		table.insert(newTable[i], v)

		-- Determine if the next index belongs on another table.
		-- If so, iterate i (current table)
		if _ + 1 > i * n then i = i + 1 end
	end
	return newTable
end

function qol.doesFileExist(_filePath)
	local f = io.open(_filePath)
	if f then f:close() return true end
	return false
end

return qol
