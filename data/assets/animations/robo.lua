return {
  ['idle'] = {
    [1] = love.graphics.newQuad(0, 0, 16, 16, 64, 64)
  },
  ['walk'] = {
    [1] = love.graphics.newQuad(0, 0, 16, 16, 64, 64),
    [2] = love.graphics.newQuad(16, 0, 16, 16, 64, 64),
    [3] = love.graphics.newQuad(32, 0, 16, 16, 64, 64),
    [4] = love.graphics.newQuad(48, 0, 16, 16, 64, 64)
  }
}