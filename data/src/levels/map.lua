local level = require(LEVELS .. 'level')

local Map = {}

local function update(self, dt)
  self.currentLevel:update(dt)
end

local function draw(self)
  self.currentLevel:draw()
end

function Map.init()
  local _map = {}

  _map.draw = draw
  _map.update = update
  
  _map.currentLevel = level.init(LEVELS .. 'debug_level')
  
  return _map
end

return Map