local vector = require(LIBS..'vector')
local entity = require(ENTITIES..'entity')
local debugUI = require(DEBUGUI..'debug_info')
local Level = {}

local function _drawDebugText(debugText)
end

local function _updateDebugString(level, player)
end

local function getGround(self)
  return self.debugFloor * self.tileHeight
end

local function update(self, dt)
  self.entities.player:update(dt, self)

  if DEBUGMODE then
    self.debugUI:update(dt)
  end
end

local function draw(self)
  self.entities.player:draw()

  if DEBUGMODE then
    self.debugUI:draw()
  end
end

function Level.init(LEVELS)
  local w = love.graphics.getWidth() / 2
  local h = love.graphics.getHeight() / 2
  
  local _level = require(LEVELS)
  _level.update = update
  _level.draw = draw
  _level.getGround = getGround

  _level.gravity = vector(0,16,0)
  _level.entities = {}
  _level.entities.player = entity.init('entityPlayer', w, h, 0)
  _level.debugUI = debugUI.init(_level, _level.entities.player)
  return _level
end

return Level