local Sprite = {}

local function _iterate(quads, dt)
  return quads.idle[1]
end


local function update(self, dt)
  self.currentQuad = _iterate(self.quads, dt)
end

local function draw(self, x, y)
  assert(type(x) == 'number')
  assert(type(y) == 'number')
  
  love.graphics.draw( self.image, self.currentQuad, x, y)
end

function Sprite.init(spriteName, imageName)
  local _sprite = {}
  _sprite.update = update
  _sprite.draw = draw

  _sprite.id = spriteName
  _sprite.image = love.graphics.newImage(SPRITES .. imageName)
  _sprite.quads = require(ANIMATIONS .. spriteName)
  _sprite.image:setFilter('nearest','nearest')
  
  print('Loaded '..spriteName..' sprite')
  
  return _sprite
end
return Sprite