local KeyControl = {}

local function _halt(dt, e)
  e:halt(dt)
  -- e.velocity.x = 0
  -- e.acceleration.x = 0
end

local function _haltJump(dt, e)
end

local function _left(dt, e)
  if e.velocity.x <= 0 then
    e.acceleration.x = (e.acceleration.x - e.speed)
  else
    _halt(dt, e)
  end
end

local function _right(dt, e)
  if e.velocity.x >= 0 then
    e.acceleration.x = (e.acceleration.x + e.speed)
  else
    _halt(dt, e)
  end
end

local function _jump(dt, e)
  e:jump()
end

function KeyControl:update(dt, entity)
  local isDown = love.keyboard.isDown

  if isDown('left') then _left(dt, entity)
  elseif isDown('right') then _right(dt, entity)
  else _halt(dt, entity)
  end

  if isDown('up') then _jump(dt, entity)
  else _haltJump(dt, entity)
  end

end

return KeyControl