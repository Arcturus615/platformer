local box = require(LIBS..'hitbox')
local graphic = require(LIBS..'graphic')
local color = require(LIBS..'color')


local DebugInfo = {}

local function _updateLevelText(level)
  if not level then return false end

  local text
  text = 'Level: '..level.name
  text = text..'\nGravity Vector: ('..tostring(level.gravity)..')'
  text = text..'\nTile Width: '..level.tileWidth
  text = text..'\nTile Height: '..level.tileHeight
  text = text..'\nTiles High: '..level.height
  text = text..'\nTiles Wide: '..level.width
  text = text..'\nRender Width: '..math.floor(level.width * level.tileWidth)
  text = text..'\nRender Height: '..math.floor(level.height*level.tileHeight) 
  text = text..'\n(debug) Floor: '..math.floor(level.debugFloor * level.tileHeight)
  return text
end

local function _updateEntityText(e)
  if not e then return false end

  local f = math.floor
  local text
  text = 'Entity: '..e.name
  text = text..'\nSprite: '..e.sprite.id
  text = text..'\nPosition: ('..tostring(e.position)..')'
  text = text..'\nVelocity: ('..tostring(e.velocity)..')'
  text = text..'\nAcceleration: ('..tostring(e.acceleration)..')'
  return text
end

local function _drawLevelText(text, box)
  if not text or not box then return false end

  graphic.rectFillLine(
    color.DARKGRAY75,
    box.x,
    box.y,
    box.w,
    box.h,
    2,
    color.WHITE
  )

  graphic.text(
    text,
    color.WHITE,
    box.x + 5,
    box.y + 5,
    1000
  )
end

local function _drawEntityText(text, box, hitBox)
  if not text or not box or not hitBox then return false end

  graphic.rectFillLine(
    color.DARKGRAY75,
    box.x,
    box.y,
    box.w,
    box.h,
    2,
    color.WHITE
  )
  graphic.rectLine(
    color.WHITE,
    hitBox.x-4,
    hitBox.y-4,
    hitBox.w+8,
    hitBox.h+8,
    1
  )
graphic.text(
  text,
  color.WHITE,
  box.x + 5,
  box.y + 5,
  1000
)
end

local function selectEntity(self, entity)
  self.entity = (entity or false)
end

local function update(self, dt)
  self.levelText = _updateLevelText(self.level)
  self.entityText = _updateEntityText(self.entity)
end

local function draw(self)
  _drawLevelText(self.levelText, self.levelBoxPosition)
  _drawEntityText(self.entityText, self.entityBoxPosition, self.entity.hitBox)
end

function DebugInfo.init(level, entity)
  local winW = love.graphics.getWidth()
  local winH = love.graphics.getHeight()

  local _debug = {}
  _debug.draw = draw
  _debug.update = update
  _debug.selectEntity = selectEntity

  _debug.entity = entity
  _debug.level = level
  _debug.entityBoxPosition =
    box.init(
    winW - (winW / 2.3),
    10,
    200,
    80)

  _debug.levelBoxPosition =
    box.init(
    10,
    10,
    150,
    150
  )

  return _debug
end

return DebugInfo