local movement = require(LOGIC .. 'key_control')

return {
  name = 'Player',
  id = 'entityPlayer',
  spriteName = 'robo',
  imageName = 'robo.png',
  jumpHeight = 40,
  speed = 8,
  movement = movement,
  w = 16,
  h = 16
}