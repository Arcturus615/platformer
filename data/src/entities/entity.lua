local sprite = require(RENDERING .. 'sprite')
local vector = require(LIBS .. 'vector')
local hitbox = require(LIBS .. 'hitbox')

local clamp = require(LIBS..'functions').clamp

local Entity = {}

local function _checkOnGround(entity, level)
  local ground = level:getGround()
  local position = entity.position
  
  position.y = clamp(0, position.y, ground)
  
  if position.y == ground then
    entity.velocity.y = 0
    entity.acceleration.y = 0

    entity.grounded = true
    entity.jumping = false
    entity.falling = false
  end
end

local function jump(self)
  if self.grounded then
    print('Beeeeep!')
    self.jumping = true
  end
end

local function halt(self, dt)
  self.acceleration.x = self.acceleration.x - self.velocity.x * 5
  if math.abs(self.acceleration.x) < 50  then
    self.acceleration.x = 0
    self.velocity.x = 0
  end
end

local function update(self, dt, level)
  assert(dt and level)
  local s = self.speed

  _checkOnGround(self, level)
  self.movement:update(dt, self)

  if self.jumping then
    self.velocity.y = self.velocity.y - (self.jumpPower * 10)
  end

  self.acceleration = self.acceleration + level.gravity 
  self.velocity = self.velocity + self.acceleration
  self.velocity:scale(dt)
  self.velocity.x = clamp( -(s*2), self.velocity.x, (s*2) )
  self.position = self.position + self.velocity

  _checkOnGround(self, level)

  self.sprite:update(dt)
  self.hitBox.x = self.position.x
  self.hitBox.y = self.position.y
end

local function draw(self)
  self.sprite:draw(
    self.position.x,
    self.position.y
    )
end

function Entity.init(entityName, x, y, z)
  assert(
    x and y and z,
    'Entity: One or more positional arguments not passed'
  )

  local entityData = require(ENTITIES .. entityName)

  assert( entityData.w, 'Entity: entityData is missing required value [w]')
  assert( entityData.h, 'Entity: entityData is missing required value [h]')
  assert( entityData.name, 'Entity: entityData is missing required value [name]')
  assert( entityData.speed, 'Entity: entityData is missing required value [speed]')
  assert( entityData.movement, 'Entity: entityData is missing required value [movement]')
  assert( entityData.imageName, 'Entity: entityData is missing required value [imageName]')
  assert( entityData.jumpHeight, 'Entity: entityData is missing required value [jumpHeight]')
  assert( entityData.spriteName, 'Entity: entityData is missing required value [spriteName]')

  local w = entityData.w
  local h = entityData.h
  local spriteName = entityData.spriteName
  local imageName = entityData.imageName

  local _entity = {}
  _entity.update = update
  _entity.draw = draw
  _entity.jump = jump
  _entity.halt = halt

  _entity.name = entityData.name
  _entity.speed = entityData.speed 
  _entity.movement = entityData.movement
  _entity.jumpPower = entityData.jumpHeight
  _entity.sprite = sprite.init(spriteName, imageName)
  
  _entity.hitBox = hitbox.init(x,y,w,h)

  _entity.position = vector(x,y)
  _entity.velocity = vector(0,0)
  _entity.acceleration = vector(0,0)
  _entity.debugString = ''
  return _entity 
end


return Entity